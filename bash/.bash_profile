#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# wayland stuff
export MOZ_ENABLE_WAYLAND=1
